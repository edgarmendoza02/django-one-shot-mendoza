from django.shortcuts import render, get_object_or_404, redirect
from todos.models   import TodoList, TodoItem
from todos.forms    import TodoListForm, EditForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()   # all objects under TodoList
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/todos.html", context)

def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    # task_details = TodoItem.objects.get(id = id) # Filter objects under TodoItem
    context = {
        "todo_list_detail" : todo_list_detail,
    }
    return render(request, "todos/detail.html", context)

def todo_list_create(request):
    if request.method == "POST": 
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save(False)
            todo_list.save()
            return redirect("todo_list_detail", id=todo_list.id) #2 direct to the to-do list for that page 
    else:
        form = TodoListForm()

    context = {
        "form" : form
    }
    return render(request, "todos/create.html", context) #3 path is create with name todo_list_create

def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance = todo_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else: 
        form = TodoListForm(instance=todo_list)

    context = {
        "form" : form,
        "todo_list_list" : todo_list,
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect(todo_list_list)
    
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST": 
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id) #2 direct to the to-do list for that page / from model
    else:
        form = TodoItemForm()

    context = {
        "form": form
    }
    return render(request, "todos/newitem.html", context) #3 path is create with name todo_list_create

def todo_item_update(request, id):
    # item_update = TodoItem.objects.get(id=id) #grab the id
    item_update = get_object_or_404(TodoItem, id=id) #errors out 404, the black 
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance =item_update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=item_update.list.id)
    else: 
        form = TodoItemForm(instance=item_update)

    context = {
        "form" : form,
    }
    return render(request, "todos/item_update.html", context)

# The todoitemform already has the information required - import that form
# use it to update the item and then create a link back to 
# TodoItem page <- probably the most difficult part 
